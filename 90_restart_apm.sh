#!/bin/bash

if [ "$APM_ENABLED" == "true" ]; then
#  if [ "$SERVICE_ENV" == "dev" ] || [ "$SERVICE_ENV" == "staging" ] || [ "$SERVICE_ENV" == "prod" ]; then
  if [ "$SERVICE_ENV" == "prod" ]; then
    echo "VALID environment found: $SERVICE_ENV"
    echo "Configure APM for service: "$SERVICE_NAME" Enviroment: "$SERVICE_ENV""
#    echo "Configure APM for service: "$SERVICE_NAME" "$SERVICE_SUFFIX" Enviroment: "$SERVICE_ENV""
#    if [ -z "$SERVICE_SUFFIX" ]; then
      CONFIGURATION=""$APM_LICENCE" "$SERVICE_NAME"-"$SERVICE_ENV""
#    else
#      CONFIGURATION=""$APM_LICENCE" "$SERVICE_NAME"-"$SERVICE_SUFFIX"-"$SERVICE_ENV""
#    fi
    PATHCURL=`find /opt/ -name '*.so*' -exec grep -q 'libcurl' {} \; -printf '%h '`
    if [[ $PATHCURL != *" "* ]]; then
      cd $PATHCURL
      LIBCURL=`find -name "*curl*" -exec basename {} +`
      ln -s $LIBCURL `echo $LIBCURL | rev | cut -c5- | rev`
    fi
    cd /opt/zpa/bin && sh configure.sh $CONFIGURATION && cd -
    echo "START APM for service: "$SERVICE_NAME"-"$SERVICE_ENV""
    cd /opt/zpa/bin/ && sh run.sh restart && cd -
  else
    echo "NOT a valid environment stoping APM"
    cd /opt/zpa/bin/ && sh run.sh stop && cd -
  fi
fi

/bin/bash
